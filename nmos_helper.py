#!/usr/bin/env python3.5
import json
import requests

def patch_multicasts(u,dd,key): 
    r = requests.patch(
            url=u,
            data=json.dumps(dd),
            headers={'Content-Type': 'application/json'})
    
    if r.status_code == 200:
        print('Successfully updated :', key)
    else:
        print('FAILED to update:', key) 
    ret = r.json()
    return ret

def change_multicasts(sender,new_state):
    for key in new_state.keys():
        url = sender.get_connection_url() + str(new_state[key]['tx_id']) + "/staged/"
        pp = {"transport_params" :  new_state[key]['transport'] }
        st = patch_multicasts(url,pp,key)
    return st
        
def hostname_to_ip (href,ip):
    href = href.split('/')
    href_ip = href[2].split(':')
    href_newip = ip.split(':')[0]
    href_ip[0] = href_newip
    href_ip = ':'.join(href_ip)
    href[2] = href_ip
    href = '/'.join(href)
    return href

# Should combine this with get_nmos_label to minimise API calls
def get_sdp_href (sender,tx_id):
    url = sender.get_node_url() + tx_id
    active = requests.get(url)
    active = active.json()
    sdp_location = hostname_to_ip(active['manifest_href'],sender.ip)
    return sdp_location


def get_sender_state(sender,sender_ids,label_truncate=0):
    state = {}
    for tx_id in sender_ids:
        label  = sender.get_nmos_label(tx_id)
        sdp_loc = get_sdp_href (sender,tx_id)
        label = label[label_truncate:]
        # print (label)
        # Reduce API calls by getting active parameters only once
        url =  sender.get_connection_url() + str(tx_id) + "/active/"
        active = requests.get(url)
        active = active.json()
        state[label] = {}
        state[label]['tx_id'] = tx_id
        state[label]['connection_status'] = active['master_enable']
        state[label]['transport'] = active['transport_params']
        state[label]['sdp_location'] = sdp_loc
    return state

def get_receiver_state(receiver,receiver_ids,label_truncate=0):
    rx_state = {}
    for tx_id in receiver_ids:
        #connection_log("*" * 72)
        #connection_log("Label for tx id:" + tx_id)
        label  = receiver.get_nmos_label(tx_id)
        label = label[label_truncate:]
        rx_state[label] = {}
        rx_state[label]['tx_id']  = tx_id
        rx_state[label]['connection_status'] = receiver.get_connection_status(tx_id)
        rx_state[label]['transport'] = receiver.get_connection_transport(tx_id)
        rx_state[label]['sdp'] = receiver.get_connection_sdp(tx_id)
    return rx_state

def get_mc_ip (state):
    # Generate a list of MCs and IPs from current state
    current_mc = []
    current_ip = []
    for key in state.keys():
        #print(key, "\t",state[key]['transport'][0]['destination_ip'],"\t", state[key]['transport'][1]['destination_ip'] )
        current_mc.append(state[key]['transport'][0]['destination_ip'])
        current_mc.append(state[key]['transport'][1]['destination_ip'])
        current_ip.append(state[key]['transport'][0]['source_ip'])
        current_ip.append(state[key]['transport'][1]['source_ip'])
    return current_mc, current_ip

# Function to generate MCs fir XIP according to MT&A Lab formula
def generate_xip_multicast(third,fourth,port):
    mc = []
    for i in [1,2,3,4]:
        mc.append('239.192.'+str(port)+'.'+str(fourth-1+i)) #red - CH1
        mc.append('239.192.'+str(port+128)+'.'+str(fourth-1+i))#blue -CH1
    for i in [1,2,3,4]:   
        mc.append('239.192.'+str(port)+'.'+str(fourth+7+i)) #red - CH2
        mc.append('239.192.'+str(port+128)+'.'+str(fourth+7+i))#blue -CH2     
    mc.append('226.192.'+str(third)+'.'+str(port)) #red - CH1
    mc.append('226.192.'+str(third+128)+'.'+str(port)) #blue -CH1
    mc.append('226.192.'+str(third+1)+'.'+str(port)) #red - CH2
    mc.append('226.192.'+str(third+1+128)+'.'+str(port)) #blue -CH2
    mc.append('225.192.'+str(third)+'.'+str(port)) #red - CH1
    mc.append('225.192.'+str(third+128)+'.'+str(port)) #blue -CH1   
    mc.append('225.192.'+str(third+1)+'.'+str(port)) #red - CH2
    mc.append('225.192.'+str(third+1+128)+'.'+str(port)) #blue -CH2   
    
    return mc

# Function to modify 40 MCs of emSFP according to EAT Lab formula
def modify_multicast(mc,ip,sender_ip,third,fourth,port):
    mc[0] = '225.192.'+str(third)+'.'+str(port)
    mc[1] = '225.192.'+str(third+128)+'.'+str(port)
    ip[0] = '192.168.105.' + sender_ip.split(".")[3]
    ip[1] = '192.168.107.' + sender_ip.split(".")[3]
    j=0
    for i in [2,4,6,8]:
        mc[i]   = '239.192.'+str(port)+'.'+str(fourth+j)
        mc[i+1] = '239.192.'+str(port+128)+'.'+str(fourth+j)
        ip[i]   = '192.168.105.' + sender_ip.split(".")[3]
        ip[i+1] = '192.168.107.' + sender_ip.split(".")[3]       
        j +=1
    mc[18] = ('226.192.'+str(third)+'.'+str(port))
    mc[19] = ('226.192.'+str(third+128)+'.'+str(port))
    ip[18] = '192.168.105.' + sender_ip.split(".")[3]
    ip[19] = '192.168.107.' + sender_ip.split(".")[3] 
    
    third +=1
    fourth +=8
    mc[20] = '225.192.'+str(third)+'.'+str(port)
    mc[21] = '225.192.'+str(third+128)+'.'+str(port)
    ip[20] = '192.168.105.' + sender_ip.split(".")[3]
    ip[21] = '192.168.107.' + sender_ip.split(".")[3]    
    j=0
    for i in [22,24,26,28]:
        mc[i]   = '239.192.'+str(port)+'.'+str(fourth+j)
        mc[i+1] = '239.192.'+str(port+128)+'.'+str(fourth+j)
        ip[i]   = '192.168.105.' + sender_ip.split(".")[3]
        ip[i+1] = '192.168.107.' + sender_ip.split(".")[3]       
        j +=1
    mc[38] = ('226.192.'+str(third)+'.'+str(port))
    mc[39] = ('226.192.'+str(third+128)+'.'+str(port))    
    ip[38] = '192.168.105.' + sender_ip.split(".")[3]
    ip[39] = '192.168.107.' + sender_ip.split(".")[3]      
    return mc,ip

# Generate new state from MC list
def generate_new_state (state,new_mc,new_ip):
    new_state = state.copy()
    j=0
    for key in new_state.keys():
        new_state[key]['transport'][0]['destination_ip'] = new_mc[j]
        new_state[key]['transport'][1]['destination_ip'] = new_mc[j+1]
        new_state[key]['transport'][0]['destination_port'] = 10000
        new_state[key]['transport'][1]['destination_port'] = 10000       
        new_state[key]['transport'][0]['source_ip'] = new_ip[j]
        new_state[key]['transport'][1]['source_ip'] = new_ip[j+1]
        new_state[key]['transport'][0]['source_port'] = 20000
        new_state[key]['transport'][1]['source_port'] = 20000        
        j +=2
    return new_state