
# How to use the AMWA NMOS IS-04/IS-05 API

## What is AMWA NMOS IS-04
AMWA NMOS IS-04 consists of three API specifications which provide the means to discover Nodes and their associated resources related to the processing of video, audio or other data. IS-04 systems are intended to enable 'zero-configuration' deployments, reducing the necessity to spend time manually configuring equipment before it is used. [More info...](https://github.com/AMWA-TV/nmos/wiki/IS-04/ "IS-04)
## What is AMWA NMOS IS-05
AMWA NMOS IS-05 is an API presented by Devices, and provides the means to create a connection between Senders and Receivers on the Devices on which it is running. This provides an inter-operable mechanism for routing media around an IP production network, taking on the role that would have previously been served by router control protocols. [More info...](https://github.com/AMWA-TV/nmos/wiki/IS-05/ "IS-05)
## Routing using IS-04/IS-05
This document is a practical guide on the useage of AMWA NMOS/IS-04/05 in the MT&A Lab. The methods presented here show an example of how AMWA NMOS IS-04/IS-05 can be used to:
* Set a `senders` Multicast parameters (covered in this document)
* Route flows from a `sender` to a `reciever`. (covered in Part 2)

The workflow presnted here is as follows:
1. Set `senders` multicast parameters
    1. Get device IP address
    2. Get IS-05 href
    3. Get flow IDS
    4. Store information required for IS-05 in a `state` variable
        1. tx_id
        2. connection status
        3. transport parameters
        4. sdp location
    5. Validate sdp (optional)
    6. Set multicast parameters of `senders` according to MT&A Lab scheme (if necessary)
    7. Activate patch
    
2. Route `senders` to `recievers` (Covered in Part 2)
    1. Patch relevant information
    2. Activate patch

## 0. Initialise librarires 
This script uses some custom built libraries and functions:
* The [NmosNode](https://github.com/pkeroulas/st2110-toolkit/tree/master/nmos/) implementation developed by [Patrick Keroulas](https://github.com/pkeroulas/) 
* The [is05control](https://github.com/AMWA-TV/nmos-testing/tree/master/utilities/is-05-control) developed by AMWA.  
* nmos_helper functions that perform json parsing and some multicast address generation +++


```python
import requests
import json
import sys
from nmos_node import NmosNode # adapted from https://github.com/pkeroulas/st2110-toolkit/tree/master/nmos
from is05Control import set_master_enable
import nmos_helper
#from requests import get, put
# See also https://github.com/AMWA-TV/nmos-testing/tree/master/utilities/is-05-control
```

## 1. Get device IP address
Manually enter the device IP address:Port of the Node.


```python
XIP_tx_ip = "192.168.2.167:4040"
XIP_rx_ip = "192.168.2.167:4040"

emSFP_tx_ip = "192.168.106.113"
emSFP_rx_ip = "192.168.106.125"

sony_tx_ip = "192.168.2.168:3001"
sony_rx_ip = "192.168.2.168:3001"

ctrl_str = 'urn:x-nmos:control:sr-ctrl'
```

### 1.1 Initialise NMOS Node class 
Three different NMOS devices have been tested in the MT&A Laboratory.
- Sony CAM
- XIP Up/down creoss converter
- Embrionix SFP gateway  

In this class implementantation, Nodes have to be identified as either `senders` or `receivers`. 

The Grass Valley XIP and Sony Camera are both a sender and receiver


```python
# Senders
XIP_tx = NmosNode(XIP_tx_ip, "tx")
emSFP_tx = NmosNode(emSFP_tx_ip, "tx")
#sony_tx = NmosNode(sony_tx_ip, "tx")
# Receivers
XIP_rx = NmosNode(XIP_rx_ip, "rx")
emSFP_rx = NmosNode(emSFP_rx_ip, "rx")
#sony_rx = NmosNode(sony_rx_ip, "rx")
```

---------------------------------------------------
## 2. Getting IS-05 href
The correct place to find where the connection-api is throught the 
[IS-04 Control Array](https://amwa-tv.github.io/nmos-device-connection-management/tags/v1.1/docs/3.1._Interoperability_-_NMOS_IS-04.html#discovery "Control Array)
See below examples of the controlls array from the 3 devices: 



```python
XIP_tx_DeviceInfo = requests.get(XIP_tx.get_devices_url())
emSFP_tx_DeviceInfo = requests.get(emSFP_tx.get_devices_url())
#sony_tx_DeviceInfo = requests.get(sony_tx.get_devices_url())

print(XIP_tx_DeviceInfo.status_code)
print(emSFP_tx_DeviceInfo.status_code)
#print(sony_tx_DeviceInfo.status_code)

XIP_tx_DeviceInfo = XIP_tx_DeviceInfo.json()
emSFP_tx_DeviceInfo = emSFP_tx_DeviceInfo.json()
#sony_tx_DeviceInfo = sony_tx_DeviceInfo.json()
```

    200
    200
    

- The Sony CAM has the connection-api at a different [ip:port] as the node api. 
- It also exposes 2 versons of the IS-05 api


```python
#print(sony_tx.get_connection_url())
#print(sony_tx.get_node_url())
#sony_tx_DeviceInfo[0]['controls']
```

- The XIP places the hostname in the controls array instead of the [ip:port]


```python
XIP_tx_DeviceInfo[0]['controls']
```




    [{'href': 'http://xip3901-136407011:4040/x-nmos/connection/v1.0/',
      'type': 'urn:x-nmos:control:sr-ctrl/v1.0'}]



- emSFP shows both the IS-05 and IS-08 apis
- emSFP also has a contol array for each channel


```python
emSFP_tx_DeviceInfo[0]['controls']
```




    [{'href': 'http://192.168.106.113:80/x-nmos/connection/v1.0/',
      'type': 'urn:x-nmos:control:sr-ctrl/v1.0'},
     {'href': 'http://192.168.106.113:80/x-nmos/channelmapping/v1.0/',
      'type': 'urn:x-nmos:control:cm-ctrl/v1.0'}]




```python
emSFP_tx_DeviceInfo[1]['controls']
```




    [{'href': 'http://192.168.106.113:80/x-nmos/connection/v1.0/',
      'type': 'urn:x-nmos:control:sr-ctrl/v1.0'},
     {'href': 'http://192.168.106.113:80/x-nmos/channelmapping/v1.0/',
      'type': 'urn:x-nmos:control:cm-ctrl/v1.0'}]



------------------------------------
## 3. Get Sender and Reciever IDs
ID's are obtained from  the connection api {/senders} or {/receivers} array  
http://{ip_address}/x-nmos/connection/v1.1/single/senders/


```python
# Senders
XIP_tx_ids = XIP_tx.get_ids()
emSFP_tx_ids = emSFP_tx.get_ids()
#sony_tx_ids = sony_tx.get_ids()
```


```python
# Receivers
XIP_rx_ids = XIP_rx.get_ids()
emSFP_rx_ids = emSFP_rx.get_ids()
#sony_rx_ids = sony_rx.get_receiver_ids()
```

-----------------------
## 4. Save state information
### 4.1. SENDER
Store information of each flow that is relevant for IS-05 connection managment in a `state` variable containing:
1. __`tx_id`__: tx_id 
2. __`connection_status`__: connection status 
3. __`transport`__: transport parameters 
4. __`sdp_location`__: SDP file `manifest_href` from IS-04 api  

The NMOS label is used as the key in a dictionary containing all the state information


```python
XIP_tx_state = nmos_helper.get_sender_state(XIP_tx,XIP_tx_ids,37)
XIP_tx_state = dict(sorted(XIP_tx_state.items()))
list(XIP_tx_state.keys())
```




    ['AES67_2',
     'AES67_3',
     'AES67_4',
     'AES67_5',
     'AES67_6',
     'AES67_7',
     'AES67_8',
     'AES67_9',
     'ANC291_10',
     'ANC291_11',
     'UniversalVideo_0',
     'UniversalVideo_1']




```python
emSFP_tx_state = nmos_helper.get_sender_state(emSFP_tx,emSFP_tx_ids,0)
list(emSFP_tx_state.keys())[:3]
```




    ['EMB JT-NM ENC-01 VidSender 000',
     'EMB JT-NM ENC-01 AudSender 010',
     'EMB JT-NM ENC-01 AudSender 020']




```python
XIP_tx_state['UniversalVideo_0']['sdp_location']
```




    'http://192.168.2.167:4040/sdps/71f552df-87ba-4583-a12f-0ddeaf8c686e.sdp'




```python
emSFP_tx_state['EMB JT-NM ENC-01 VidSender 100']['transport']
```




    [{'source_ip': '192.168.1.1',
      'destination_ip': '239.0.2.2',
      'source_port': 10000,
      'destination_port': 20000,
      'rtp_enabled': False},
     {'source_ip': '192.168.1.1',
      'destination_ip': '239.0.2.3',
      'source_port': 10000,
      'destination_port': 20000,
      'rtp_enabled': False}]



-----------------------
### 4.2 Save RECEIVER state information
Store information of each flow that is relevant for IS-05 connection managment information in a `state` variable containing:
1. __`tx_id`__: tx_id 
2. __`connection_status`__: connection status 
3. __`transport`__: transport parameters 
4. __`sdp`__: raw SDP data from the /active connection IS-05 api. 


#### For XIP


```python
XIP_rx_state = nmos_helper.get_receiver_state(XIP_rx,XIP_rx_ids,37)
```


```python
for key in XIP_rx_state.keys():
    print(key, "\t",XIP_rx_state[key]['connection_status'] )
```

    UNIVERSAL_0 	 True
    ANC291_10 	 True
    ANC291_11 	 True
    UNIVERSAL_1 	 True
    AES67_4 	 False
    AES67_9 	 False
    AES67_3 	 True
    AES67_7 	 True
    AES67_6 	 True
    AES67_8 	 False
    AES67_2 	 True
    AES67_5 	 False
    


```python
XIP_rx_state['UNIVERSAL_0']['transport']
```




    [{'source_ip': None,
      'multicast_ip': None,
      'interface_ip': '0.0.0.0',
      'destination_port': 'auto',
      'rtp_enabled': True},
     {'source_ip': None,
      'multicast_ip': None,
      'interface_ip': '0.0.0.0',
      'destination_port': 'auto',
      'rtp_enabled': True}]



#### For emSFP receiver


```python
emSFP_rx_state = nmos_helper.get_receiver_state(emSFP_rx,emSFP_rx_ids)
```


```python
for key in emSFP_rx_state.keys():
    print(key, "\t",emSFP_rx_state[key]['connection_status'] )
```

    EMB JT-NM DEC-02 VidRecv 000 	 False
    EMB JT-NM DEC-02 AudRecv 010 	 False
    EMB JT-NM DEC-02 AudRecv 020 	 False
    EMB JT-NM DEC-02 AudRecv 030 	 False
    EMB JT-NM DEC-02 AudRecv 040 	 False
    EMB JT-NM DEC-02 AncRecv 050 	 False
    EMB JT-NM DEC-02 VidRecv 100 	 False
    EMB JT-NM DEC-02 AudRecv 110 	 False
    EMB JT-NM DEC-02 AudRecv 120 	 False
    EMB JT-NM DEC-02 AudRecv 130 	 False
    EMB JT-NM DEC-02 AudRecv 140 	 False
    EMB JT-NM DEC-02 AncRecv 150 	 False
    

### Transportfile locaction for a receiver
The XIP receiver does not store the __`transport_file`__ data in the `/connection/v1.0/bulk/receivers/{flow_ID}/active`  
The emSFP includes __`transport_file`__ data.  
The __`transport_params`__ take precednce over the __`transport_file`__ information when there is a disagreement. [See reference](https://amwa-tv.github.io/nmos-device-connection-management/tags/v1.1/docs/2.2._APIs_-_Server_Side_Implementation.html#staged-requests-precedence) This may be a reason why the XIP `reciever` does not display the __`transport_file`__ . 


```python
print(XIP_rx_state['UNIVERSAL_1']['sdp'])
```

    None
    


```python
emSFP_rx_state['EMB JT-NM DEC-02 VidRecv 100']['sdp']
```




    'v=0\r\no=- 1443716955 1443716955 IN IP4 192.168.106.125\r\ns=JT-NM DEC-02_1-0-0\r\nt=0 0\r\na=group:DUP primary secondary\r\nm=video 20000 RTP/AVP 0\r\nc=IN IP4 225.192.11.30/64\r\na=source-filter: incl IN IP4 225.192.11.30 192.168.105.117\r\na=rtpmap:0 raw/90000\r\na=fmtp:0 sampling=YCbCr-4:2:2; width=1920; height=1080; exactframerate=30000/1001; depth=10; TCS=SDR; colorimetry=BT709; PM=2110GPM; SSN=ST2110-20:2017; TP=2110TPN; interlace; \r\na=mediaclk:direct=0\r\na=ts-refclk:ptp=IEEE1588-2008:08-00-11-FF-FE-22-3F-B8:127\r\na=mid:primary\r\nm=video 20000 RTP/AVP 0\r\nc=IN IP4 225.192.139.30/64\r\na=source-filter: incl IN IP4 225.192.139.30 192.168.107.117\r\na=rtpmap:0 raw/90000\r\na=fmtp:0 sampling=YCbCr-4:2:2; width=1920; height=1080; exactframerate=30000/1001; depth=10; TCS=SDR; colorimetry=BT709; PM=2110GPM; SSN=ST2110-20:2017; TP=2110TPN; interlace; \r\na=mediaclk:direct=0\r\na=ts-refclk:ptp=IEEE1588-2008:08-00-11-FF-FE-22-3F-B8:127\r\na=mid:secondary\r\n'



-----------------------------------------
## 5. Running sdpoker Node.js (optional)
Verify the validity of the sender `sdp` files using AMWA/sdpoker  
To install sdpoker use:  
`npm install -g AMWA-TV/sdpoker`


```python
!sdpoker --version
```

    0.1.0
    

### In NMOS:
- senders have a "manifest_href" with a link to sdp file
- some receivers paste the contents of the sdp file into "transport_params/data" 
- sdpoker wants a URL or file
- sdpoker only displays a result if it finds errors.


```python
!sdpoker --should --noMedia --duplicate --unicast {XIP_tx_state['UniversalVideo_0']['sdp_location']}
```

    Found 2 error(s) in SDP file:
    1: Line 9: Unicast connections requested by configuration and connection data field address '225.192.0.14 is multicast.
    2: Line 17: Unicast connections requested by configuration and connection data field address '225.192.128.14 is multicast.
    

--------------------
## 6. Set multicast parameters of senders
### 6.1. Store current Multicast information
Save the current Multicast information to compare with the expected information according to the MT&A Lab Multicast reservation sheet.
If the Sender MC Information is different than expected, IS-05 will be used to patch the correct MC information.


```python
emSFP_current_mc, emSFP_current_ip = nmos_helper.get_mc_ip (emSFP_tx_state)
```


```python
print(emSFP_current_mc[:6])
print(emSFP_current_ip[:6])
```

    ['239.0.1.2', '239.0.1.3', '239.0.1.4', '239.0.1.5', '239.0.1.6', '239.0.1.7']
    ['192.168.0.1', '192.168.0.1', '192.168.0.1', '192.168.0.1', '192.168.0.1', '192.168.0.1']
    


```python
XIP_current_mc, XIP_current_ip = nmos_helper.get_mc_ip (XIP_tx_state)
```


```python
print(XIP_current_mc[:6])
print(XIP_current_ip[:6])
```

    ['239.192.14.1', '239.192.142.1', '239.192.14.2', '239.192.142.2', '239.192.14.3', '239.192.142.3']
    ['192.168.104.53', '192.168.108.53', '192.168.104.53', '192.168.108.53', '0.0.0.0', '0.0.0.0']
    

-----------------------------
### Multicast formula in MT&A lab
Multicast assignment is based on switch and port number

|  | Name         | IP | Video Ch1 <br /> 3rd octet  | Video Ch2 <br />3rd octet  |  Video <br />4th octet  |\| | Audio <br /> 3rd octet  | Audio Ch1 <br /> 4th octet | Audio Ch2 <br /> 4th octet |
|--|:------------|----|--- |--  |:---: | --| :---:| ---| --|
|1 |aggregator    | #.#.#.8  | 10 | 11 | port |\| |port  |101-108 | 109-116 |
|2 |lf1-deep      | #.#.#.7  |  0 |  1 | port |\| |port  |001-008 | 009-016 |
|3 |lf2-shallow   | #.#.#.41 | 20 | 21 | port |\| |port  |021-028 | 029-036 |
|4 |alf10-audio   | #.#.#.10 | 40 | 41 | port |\| |port  |041-048 | 049-056 |

--------
Each device has a unique Multicast assignment strategy. The simplest solution is to auto-assign MC addresses sequentially.  
A device specific MC assignment strategy is used in the MT&A lab, and it makes it easy to determine MC assignment based on:
- Switch
- Switch port numnber
- Flow type


```python
XIP_new_mc = nmos_helper.generate_xip_multicast (0,1,14)
```


```python
emSFP_new_mc = emSFP_current_mc.copy()
emSFP_new_ip = emSFP_current_ip.copy()
emSFP_new_mc,emSFP_new_ip = nmos_helper.modify_multicast(emSFP_new_mc,emSFP_new_ip,emSFP_tx_ip,10,101,29)
```

### 6.2 Generate New state 
Use the Multicat address array to populate a new state variable


```python
XIP_tx_new_state = nmos_helper.generate_new_state(XIP_tx_state,XIP_new_mc,XIP_current_ip)
```


```python
emSFP_tx_new_state = nmos_helper.generate_new_state(emSFP_tx_state,emSFP_new_mc,emSFP_new_ip)
```

### 6.3 Verify MC
Verify the correct MC addresses have been generated


```python
new_state = emSFP_tx_new_state
for key in new_state.keys():
    print(key, "\t", new_state[key]['transport'][0]['source_ip'],"\t", 
          new_state[key]['transport'][0]['destination_ip'],"\n\t\t\t\t",
          new_state[key]['transport'][1]['source_ip'],"\t",
          new_state[key]['transport'][1]['destination_ip'])
```

    EMB JT-NM ENC-01 VidSender 000 	 192.168.105.113 	 225.192.10.29 
    				 192.168.107.113 	 225.192.138.29
    EMB JT-NM ENC-01 AudSender 010 	 192.168.105.113 	 239.192.29.101 
    				 192.168.107.113 	 239.192.157.101
    EMB JT-NM ENC-01 AudSender 020 	 192.168.105.113 	 239.192.29.102 
    				 192.168.107.113 	 239.192.157.102
    EMB JT-NM ENC-01 AudSender 030 	 192.168.105.113 	 239.192.29.103 
    				 192.168.107.113 	 239.192.157.103
    EMB JT-NM ENC-01 AudSender 040 	 192.168.105.113 	 239.192.29.104 
    				 192.168.107.113 	 239.192.157.104
    EMB JT-NM ENC-01 AudSender 050 	 192.168.0.1 	 239.0.1.12 
    				 192.168.0.1 	 239.0.1.13
    EMB JT-NM ENC-01 AudSender 060 	 192.168.0.1 	 239.0.1.14 
    				 192.168.0.1 	 239.0.1.15
    EMB JT-NM ENC-01 AudSender 070 	 192.168.0.1 	 239.0.1.16 
    				 192.168.0.1 	 239.0.1.17
    EMB JT-NM ENC-01 AudSender 080 	 192.168.0.1 	 239.0.1.18 
    				 192.168.0.1 	 239.0.1.19
    EMB JT-NM ENC-01 AncSender 090 	 192.168.105.113 	 226.192.10.29 
    				 192.168.107.113 	 226.192.138.29
    EMB JT-NM ENC-01 VidSender 100 	 192.168.105.113 	 225.192.11.29 
    				 192.168.107.113 	 225.192.139.29
    EMB JT-NM ENC-01 AudSender 110 	 192.168.105.113 	 239.192.29.109 
    				 192.168.107.113 	 239.192.157.109
    EMB JT-NM ENC-01 AudSender 120 	 192.168.105.113 	 239.192.29.110 
    				 192.168.107.113 	 239.192.157.110
    EMB JT-NM ENC-01 AudSender 130 	 192.168.105.113 	 239.192.29.111 
    				 192.168.107.113 	 239.192.157.111
    EMB JT-NM ENC-01 AudSender 140 	 192.168.105.113 	 239.192.29.112 
    				 192.168.107.113 	 239.192.157.112
    EMB JT-NM ENC-01 AudSender 150 	 192.168.1.1 	 239.0.2.12 
    				 192.168.1.1 	 239.0.2.13
    EMB JT-NM ENC-01 AudSender 160 	 192.168.1.1 	 239.0.2.14 
    				 192.168.1.1 	 239.0.2.15
    EMB JT-NM ENC-01 AudSender 170 	 192.168.1.1 	 239.0.2.16 
    				 192.168.1.1 	 239.0.2.17
    EMB JT-NM ENC-01 AudSender 180 	 192.168.1.1 	 239.0.2.18 
    				 192.168.1.1 	 239.0.2.19
    EMB JT-NM ENC-01 AncSender 190 	 192.168.105.113 	 226.192.11.29 
    				 192.168.107.113 	 226.192.139.29
    


```python
new_state = XIP_tx_new_state
for key in new_state.keys():
    print(key, "\t", new_state[key]['transport'][0]['source_ip'],"\t", 
          new_state[key]['transport'][0]['destination_ip'],"\n\t\t",
          new_state[key]['transport'][1]['source_ip'],"\t",
          new_state[key]['transport'][1]['destination_ip'])
```

    AES67_2 	 192.168.104.53 	 239.192.14.1 
    		 192.168.108.53 	 239.192.142.1
    AES67_3 	 192.168.104.53 	 239.192.14.2 
    		 192.168.108.53 	 239.192.142.2
    AES67_4 	 0.0.0.0 	 239.192.14.3 
    		 0.0.0.0 	 239.192.142.3
    AES67_5 	 0.0.0.0 	 239.192.14.4 
    		 0.0.0.0 	 239.192.142.4
    AES67_6 	 0.0.0.0 	 239.192.14.9 
    		 0.0.0.0 	 239.192.142.9
    AES67_7 	 0.0.0.0 	 239.192.14.10 
    		 0.0.0.0 	 239.192.142.10
    AES67_8 	 0.0.0.0 	 239.192.14.11 
    		 0.0.0.0 	 239.192.142.11
    AES67_9 	 0.0.0.0 	 239.192.14.12 
    		 0.0.0.0 	 239.192.142.12
    ANC291_10 	 0.0.0.0 	 226.192.0.14 
    		 0.0.0.0 	 226.192.128.14
    ANC291_11 	 0.0.0.0 	 226.192.1.14 
    		 0.0.0.0 	 226.192.129.14
    UniversalVideo_0 	 192.168.104.53 	 225.192.0.14 
    		 192.168.108.53 	 225.192.128.14
    UniversalVideo_1 	 192.168.104.53 	 225.192.1.14 
    		 192.168.108.53 	 225.192.129.14
    

### 6.4 Patch Multicasts
If the `sender` multicast assignment does not match what is expected in the MT&A Lab... Patch the correct Multicast using IS-05.  
Patching is done in the staged endpoint `http://{ip}:{port}/x-nmos/connection/{ver}/single/receivers/{flowID}/staged`
#### For XIP


```python
# Apply patch if there is a change in MC addresses
if (XIP_current_mc != XIP_new_mc):
    print("Changing MC addresses")
    res = change_multicasts(XIP_tx,XIP_tx_new_state)
else:
    print("No change in Multicast addresses")
```

    No change in Multicast addresses
    

#### For emSFP


```python
# Apply patch if there is a change in MC addresses
if (emSFP_current_mc != emSFP_new_mc):
    print("Changing MC addresses")
    res = nmos_helper.change_multicasts(emSFP_tx,emSFP_tx_new_state)
else:
    print("No change in Multicast addresses")
```

    Changing MC addresses
    Successfully updated : EMB JT-NM ENC-01 VidSender 000
    Successfully updated : EMB JT-NM ENC-01 AudSender 010
    Successfully updated : EMB JT-NM ENC-01 AudSender 020
    Successfully updated : EMB JT-NM ENC-01 AudSender 030
    Successfully updated : EMB JT-NM ENC-01 AudSender 040
    Successfully updated : EMB JT-NM ENC-01 AudSender 050
    Successfully updated : EMB JT-NM ENC-01 AudSender 060
    Successfully updated : EMB JT-NM ENC-01 AudSender 070
    Successfully updated : EMB JT-NM ENC-01 AudSender 080
    Successfully updated : EMB JT-NM ENC-01 AncSender 090
    Successfully updated : EMB JT-NM ENC-01 VidSender 100
    Successfully updated : EMB JT-NM ENC-01 AudSender 110
    Successfully updated : EMB JT-NM ENC-01 AudSender 120
    Successfully updated : EMB JT-NM ENC-01 AudSender 130
    Successfully updated : EMB JT-NM ENC-01 AudSender 140
    Successfully updated : EMB JT-NM ENC-01 AudSender 150
    Successfully updated : EMB JT-NM ENC-01 AudSender 160
    Successfully updated : EMB JT-NM ENC-01 AudSender 170
    Successfully updated : EMB JT-NM ENC-01 AudSender 180
    Successfully updated : EMB JT-NM ENC-01 AncSender 190
    

#### Verify Patch
- Check the staged endpoint to confirm patch if it was applied


```python
print(XIP_tx.get_connection_url() + str(XIP_tx_new_state['UniversalVideo_0']['tx_id']) + "/staged/")
print(emSFP_tx.get_connection_url() + str(emSFP_tx_new_state['EMB JT-NM ENC-01 VidSender 000']['tx_id']) + "/staged/")
```

    http://192.168.2.167:4040/x-nmos/connection/v1.0/single/senders/71f552df-87ba-4583-a12f-0ddeaf8c686e/staged/
    http://192.168.106.113:80/x-nmos/connection/v1.0/single/senders/ecff21df-7bfd-25de-9805-40a36ba03bc8/staged/
    

## 7. Activate patch
### 7.1 Activating XIP senders

Activation is not necessary if the multiccasts did not change in Step 6 step 6.


```python
for key in XIP_tx_new_state.keys():
    url = XIP_tx.get_connection_url() + str(new_state[key]['tx_id']) + "/staged/"
    set_master_enable(url,True)
```

    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    

### 7.2 Activating emSFP Senders
- Select Video, Audio 1-4, ANC for activation.
- Do not activate Audio 5-8


```python
new_state = emSFP_tx_new_state
active_flows = {}
exclude_flows= ["50","60","70","80"]
for key in new_state.keys():
    if not any([ex in key for ex in exclude_flows]):
        print(key, new_state[key]['transport'][0]['destination_ip'],"\t", 
              new_state[key]['transport'][1]['destination_ip'] ,"\t",
              new_state[key]['connection_status'])
        active_flows[key] = new_state[key]
```

    EMB JT-NM ENC-01 VidSender 000 225.192.10.29 	 225.192.138.29 	 False
    EMB JT-NM ENC-01 AudSender 010 239.192.29.101 	 239.192.157.101 	 False
    EMB JT-NM ENC-01 AudSender 020 239.192.29.102 	 239.192.157.102 	 False
    EMB JT-NM ENC-01 AudSender 030 239.192.29.103 	 239.192.157.103 	 False
    EMB JT-NM ENC-01 AudSender 040 239.192.29.104 	 239.192.157.104 	 False
    EMB JT-NM ENC-01 AncSender 090 226.192.10.29 	 226.192.138.29 	 False
    EMB JT-NM ENC-01 VidSender 100 225.192.11.29 	 225.192.139.29 	 False
    EMB JT-NM ENC-01 AudSender 110 239.192.29.109 	 239.192.157.109 	 False
    EMB JT-NM ENC-01 AudSender 120 239.192.29.110 	 239.192.157.110 	 False
    EMB JT-NM ENC-01 AudSender 130 239.192.29.111 	 239.192.157.111 	 False
    EMB JT-NM ENC-01 AudSender 140 239.192.29.112 	 239.192.157.112 	 False
    EMB JT-NM ENC-01 AncSender 190 226.192.11.29 	 226.192.139.29 	 False
    


```python
for key in active_flows.keys():
    url = emSFP_tx.get_connection_url() + str(active_flows[key]['tx_id']) + "/staged/"
    set_master_enable(url,True)
```

    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    Setting master_enable: True
    Successful request
    

### 7.3 Confirm success by checking NMOS and emSET status


```python
key_test = 'EMB JT-NM ENC-01 AudSender 120'
sender = emSFP_tx
new_state = emSFP_tx_new_state
url_stgaged = sender.get_connection_url() + str(new_state[key_test]['tx_id']) + "/staged/"
url_active = sender.get_connection_url() + str(new_state[key_test]['tx_id']) + "/active/"
emset = 'http://localhost:8080'
print(url_active)
print(emset)
```

    http://192.168.106.113:80/x-nmos/connection/v1.0/single/senders/ffc7f184-ff4b-3650-ae68-40a36ba03bc8/active/
    http://localhost:8080
    


```python
act_test = requests.get(url_active)
print(act_test.status_code)
act_test.json()
```

    200
    




    {'receiver_id': None,
     'master_enable': True,
     'activation': {'mode': 'activate_immediate',
      'requested_time': None,
      'activation_time': '2776:413000'},
     'transport_params': [{'source_ip': '192.168.105.113',
       'destination_ip': '239.192.29.110',
       'source_port': 20000,
       'destination_port': 10000,
       'rtp_enabled': True},
      {'source_ip': '192.168.107.113',
       'destination_ip': '239.192.157.110',
       'source_port': 20000,
       'destination_port': 10000,
       'rtp_enabled': True}]}



## Using is05-control from AMWA testing (stay tuned for Part 2)


```python
%run is05Control.py -h
```

    usage: is05Control.py [-h] --ip IP [--port PORT] [--version VERSION] [-s] [-r]
                          [--request REQUEST] [--sdp SDP] -u UUID
    
    optional arguments:
      -h, --help            show this help message and exit
      --ip IP               IP address or Hostname of DuT
      --port PORT           Port number of IS-05 API of DuT
      --version VERSION     Version of IS-05 API of DuT
      -s, --sender          Configure NMOS Sender
      -r, --receiver        Configure NMOS Receiver
      --request REQUEST     JSON data to be sent in the request to configure
                            sender
      --sdp SDP             SDP file to be sent in the request to configure
                            receiver
      -u UUID, --uuid UUID  UUID of resource to be configured
    
