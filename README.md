# Using NMOS
This jupyter notebook is used to demonstrate some of the functionality provided by the [AMWA NMOS Specification](https://www.amwa.tv/nmos).
A rendering of this notebook is available in an [article on confluence](https://cbcradiocanada.atlassian.net/l/c/zA1HBkwb) or in the `export-as/` folder

### Using this Notebook
* Log into the ASD Lab vpn
* Ensure the NMOS devices are on the network and reserved for your testing
* Have fun! and suggest improvements.

### Who do I talk to? ###

* me
* asd-lab-team 
